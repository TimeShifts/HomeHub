import { HomeHubPage } from './app.po';

describe('home-hub App', () => {
  let page: HomeHubPage;

  beforeEach(() => {
    page = new HomeHubPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
