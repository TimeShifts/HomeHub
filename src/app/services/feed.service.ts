import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import { JsonFeed } from '../model/feed';

//import { Feed } from './model/feed';
export enum FeedTopic {
    tech = <any>"https://www.reddit.com/r/tech/.json" ,
    gaming = <any>"https://www.reddit.com/r/gaming/.json",
    science = <any>"https://www.reddit.com/r/science/.json",
    reddit = <any>"https://www.reddit.com/.json",
    news = <any>"https://www.reddit.com/r/news/.json",
    esports = <any>"https://www.reddit.com/r/overwatch/.json"
}

@Injectable()
export class FeedService {
  private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';
  private feedUrl: string = 'https://www.reddit.com/.rss?feed=1a0febe2893b14e23a1830770b1ab2ebc688f44c&user=SomeoneYouMayNotKnow';
  public feed: Observable<any>;
  private topics = [{value:"tech", name: "Tech"}, {value:"gaming", name: "Gaming"}, {value:"science", name: "Science"}, {value:"news", name: "News"}, {value:"reddit", name: "Reddit Front Page"}, {value:"esports", name: "Overwatch"}];

  constructor(private http: Http) { 
    
  }

  getTopics(){
    return this.topics;
  }

  getFeedContent() {
    if(this.feedUrl.substring(this.feedUrl.length - 4) == "json"){
      return this.http.get(this.feedUrl)
            .map(this.extractJsonFeeds)
            .catch(this.handleError);
    }
  }
  
  switchFeed(topic: string){
    this.feedUrl = FeedTopic[topic];
  }

  public extractJsonFeeds(res: Response): any {
    console.log("refresh feed", res.json());
    let feed = res.json();
    let jsonFeed = [];

    let count=0;
    for(let feedItem of feed.data.children){
      jsonFeed.push(new JsonFeed(feedItem.data));
      count++;
    }
    return jsonFeed || { };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
