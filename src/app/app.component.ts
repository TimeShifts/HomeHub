declare var particlesJS: any;
import { Component, OnInit } from '@angular/core';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  errorMessage: any;
  weatherData: any;

  constructor (public weatherService: WeatherService) {
    particlesJS.load('particles-js', '/assets/particles.json', function() { });
  }

  ngOnInit() {
    this.getWeather();
  }

  getWeather(){
     this.weatherService.getWeatheritemsbyCity().then(data => {
        this.weatherData = data; 
        console.log(data);
      },error =>  this.errorMessage = <any>error           
     );
  }
}

