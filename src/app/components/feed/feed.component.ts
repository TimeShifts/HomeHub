import { Component, OnInit } from '@angular/core';
import { FeedService } from '../../services/feed.service';
import { Subscription } from 'rxjs/Subscription';
import { DomSanitizer } from '@angular/platform-browser';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {
  private feeds: any=[];
  private allFeeds: any=[];
  private _feedSubscription: Subscription;
  private remainingFeedItems: number;
  private displayNum: number = 4;
  private topicIndex: number = 0;
  private topics: any;
  private currentTopic:any;

  constructor(public feedService: FeedService, private sanitizer: DomSanitizer) { 
    this.topics = feedService.getTopics();
    this.getNewTopicFeed(this.topics[2]);

    // Get new feed every minute
    let timer = Observable.timer(1000 * 20,1000 * 20);
    timer.subscribe( feed => { this.displayNextItems();});
  }

  ngOnInit() {
    
  }

  onChange(newObj) {
    console.log(newObj);
    this.getNewTopicFeed(newObj.target);
  }

  private getNewTopicFeed(topic) {
    console.log(topic);
    this.currentTopic = topic;
    this.feedService.switchFeed(this.currentTopic.value);
    this.feedService.getFeedContent().subscribe(
      feed => {
        this.allFeeds = [];
        this.remainingFeedItems = feed.length;
        for(let feedItem of feed){
            if(feedItem.html){
              feedItem.html = this.sanitizeHtml(feedItem.html);
            }
            this.allFeeds.push(feedItem);
          }
        this.displayNextItems();
        },
      error => console.log(error));
  }

  private displayNextItems(){
    if(this.remainingFeedItems >= this.displayNum){
      this.feeds = this.allFeeds.splice(1,this.displayNum);
      console.log(this.feeds);
      this.remainingFeedItems = this.remainingFeedItems - this.displayNum;
    }else{
      console.log("next topic");
      this.topicIndex = this.topicIndex+1;
      if(this.topicIndex >= this.topics.length){
        this.topicIndex = 0;
      }
      this.getNewTopicFeed(this.topics[this.topicIndex]);
    }
  }

  public sanitizeHtml(html): any{
    html = this.unescapeHtml(html);
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  unescapeHtml(text) {
    return text
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&#039;/g, '\'');
  }

}
