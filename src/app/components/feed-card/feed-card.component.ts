import { Component, OnInit, Input, Output} from '@angular/core';

@Component({
  selector: 'feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.css']
})
export class FeedCardComponent implements OnInit {

  @Input() feed: any;
  
  constructor() { }

  ngOnInit() {
     
  }

  goToUrl(url){
     window.open(url, "_blank");
  }

}
