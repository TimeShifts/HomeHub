import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

export class EscapeHtmlPipe {
  constructor(private sanitizer: DomSanitizer) {
  }

  safeHtml(content) {
    console.log("sanitizing");
    return this.sanitizer.bypassSecurityTrustHtml(content);
  }
}

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
} 