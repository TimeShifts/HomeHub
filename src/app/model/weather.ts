export enum WeatherIcon {
    clearsky = <any>"mdi ghost-icon-weather mdi-weather-sunny" ,
    fewclouds = <any>"mdi ghost-icon-weather mdi-weather-cloudy",
    scatteredclouds = <any>"mdi ghost-icon-weather mdi-weather-partlycloudy",
    brokenclouds = <any>"mdi ghost-icon-weather mdi-weather-cloudy",
    showerrain = <any>"mdi ghost-icon-weather mdi-weather-rainy",
    rain = <any>"mdi ghost-icon-weather mdi-weather-pouring",
    thunderstorm = <any>"mdi ghost-icon-weather mdi-weather-lightning-rainy",
    snow = <any>"mdi ghost-icon-weather mdi-weather-snowy",
    mist = <any>"mdi ghost-icon-weather mdi-weather-fog"
}

export class Weather{
   public icon: string = "";
   constructor(
               public city: string, 
               public description: string,
               public temp: number, 
               public high: number,
               public low: number,
               public time: string){
                     this.city = city;
                     this.description = description;
                     this.temp = temp;
                     this.low = low;
                     this.high = high;
                     this.time = time;
                     console.log(WeatherIcon[this.removeSpace(description)]);
                     this.icon = WeatherIcon[this.removeSpace(description)];
               } 
      
  removeSpace(description){
      return description.replace(" ", "");
  }
 }

 export class DailyWeather{
        public icon: string = "";
   constructor(
               public city: string, 
               public description: string,
               public temp: number, 
               public high: number,
               public low: number,
               public time: string){
                     this.city = city;
                     this.description = description;
                     this.temp = temp;
                     this.low = low;
                     this.high = high;
                     this.time = time;
                     this.icon = WeatherIcon[this.removeSpace(description)];
               } 

removeSpace(description){
      return description.replace(" ", "");
  }
 }